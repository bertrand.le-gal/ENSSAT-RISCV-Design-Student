-- Testbench created online at:
--   https://www.doulos.com/knowhow/perl/vhdl-testbench-creation-using-perl/
-- Copyright Doulos Ltd

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity immediate_tb is
end;

architecture bench of immediate_tb is

  component immediate
  Port ( 
     INSTR    : in  STD_LOGIC_VECTOR (31 downto 0);
     isStore  : in  STD_LOGIC;
     isLoad   : in  STD_LOGIC;
     isbranch : in  STD_LOGIC;
     isJAL    : in  STD_LOGIC;
     isAuipc  : in  STD_LOGIC;
     isLui    : in  STD_LOGIC;
     imm      : out STD_LOGIC_VECTOR (31 downto 0)
   );
  end component;

  signal INSTR: STD_LOGIC_VECTOR (31 downto 0);
  signal isStore: STD_LOGIC;
  signal isLoad: STD_LOGIC;
  signal isbranch: STD_LOGIC;
  signal isJAL: STD_LOGIC;
  signal isAuipc: STD_LOGIC;
  signal isLui: STD_LOGIC;
  signal imm: STD_LOGIC_VECTOR (31 downto 0) ;

begin

  uut: immediate port map ( INSTR    => INSTR,
                            isStore  => isStore,
                            isLoad   => isLoad,
                            isbranch => isbranch,
                            isJAL    => isJAL,
                            isAuipc  => isAuipc,
                            isLui    => isLui,
                            imm      => imm );

  stimulus: process
  begin
  
    -- Put initialisation code here


    -- Put test bench stimulus code here

    wait;
  end process;


end;


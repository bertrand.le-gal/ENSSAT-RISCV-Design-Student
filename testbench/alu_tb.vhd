-- Testbench created online at:
--   https://www.doulos.com/knowhow/perl/vhdl-testbench-creation-using-perl/
-- Copyright Doulos Ltd

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity alu_tb is
end;

architecture bench of alu_tb is

  component alu
  Port ( 
     rs1_v             : in  STD_LOGIC_VECTOR (31 downto 0);
     rs2_v             : in  STD_LOGIC_VECTOR (31 downto 0);
     isALUreg          : in  STD_LOGIC;
     isBranch          : in  STD_LOGIC;
     isAluSubstraction : in  STD_LOGIC;
     func3             : in  STD_LOGIC_VECTOR ( 2 downto 0);
     imm_v             : in  STD_LOGIC_VECTOR (31 downto 0);
     aluOut_v          : out STD_LOGIC_VECTOR (31 downto 0);
     aluPlus_v         : out STD_LOGIC_VECTOR (31 downto 0);
     takeBranch        : out STD_LOGIC
   );
  end component;

  signal rs1_v: STD_LOGIC_VECTOR (31 downto 0);
  signal rs2_v: STD_LOGIC_VECTOR (31 downto 0);
  signal isALUreg: STD_LOGIC;
  signal isBranch: STD_LOGIC;
  signal isAluSubstraction: STD_LOGIC;
  signal func3: STD_LOGIC_VECTOR ( 2 downto 0);
  signal imm_v: STD_LOGIC_VECTOR (31 downto 0);
  signal aluOut_v: STD_LOGIC_VECTOR (31 downto 0);
  signal aluPlus_v: STD_LOGIC_VECTOR (31 downto 0);
  signal takeBranch: STD_LOGIC ;

begin

  uut: alu port map ( rs1_v             => rs1_v,
                      rs2_v             => rs2_v,
                      isALUreg          => isALUreg,
                      isBranch          => isBranch,
                      isAluSubstraction => isAluSubstraction,
                      func3             => func3,
                      imm_v             => imm_v,
                      aluOut_v          => aluOut_v,
                      aluPlus_v         => aluPlus_v,
                      takeBranch        => takeBranch );

  stimulus: process
  begin
  
    -- Put initialisation code here


    -- Put test bench stimulus code here

    wait;
  end process;


end;


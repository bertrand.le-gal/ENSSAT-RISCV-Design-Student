library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity mem_ram is
    GENERIC(
        RAM_ADDR_BITS : INTEGER := 14;
        filename      : STRING  := "../../soft/PROGROM.hex32"
    );
    Port ( 
        CLOCK   : IN  STD_LOGIC;
        ADDR_RW : IN  STD_LOGIC_VECTOR(RAM_ADDR_BITS-1 DOWNTO 0);
        ENABLE  : IN  STD_LOGIC;
        WRITE_M : IN  STD_LOGIC_VECTOR( 3 DOWNTO 0);
        DATA_W  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
        DATA_R  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
     );
end mem_ram;


architecture arch of mem_ram is 

    TYPE   ram_type IS ARRAY (0 TO ((2**RAM_ADDR_BITS)-1)) OF STD_LOGIC_VECTOR (31 DOWNTO 0);

    impure function InitRomFromFile(RamFileName : in string)
      return ram_type is
         file RamFile         : text open read_mode is RamFileName;
         variable RamFileLine : line;
         variable RAM         : ram_type;
    begin
      for I in ram_type'range loop
         readline(RamFile, RamFileLine);
         hread(RamFileLine, RAM(I));
      end loop;
      return RAM;
    end function;

    SIGNAL memory : ram_type := InitRomFromFile( filename );

begin

    --
    --
    --

end arch;
 
